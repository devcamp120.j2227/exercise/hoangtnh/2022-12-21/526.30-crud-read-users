import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { editUserHandler, changeStatusEditUser, setStatus } from '../actions/users.action';
import { Grid, TextField, InputLabel, Select, MenuItem, Button, Box, Typography, Modal} from '@mui/material';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};
export default function EditUserModal() {
    const {openEdit, user} = useSelector((reduxData)=> reduxData.userReducer);
    console.log("edit",user)
    const dispatch = useDispatch();
    const [editUser, handleEditUser] = useState({
      id: user.id,
      firstname: user.firstname,
      lastname: user.lastname,
      subject: user.subject,
      country: user.country,
      registerStatus: user.registerStatus,
      customerType: user.customerType
    })
    const handleClose = () => {
        dispatch(changeStatusEditUser(false))
    }
    const handleChange = (event) => {
        const value = event.target.value;
        handleEditUser({
          ...editUser,
          [event.target.name] : value
        })
      };
    const onBtnEditUserHandler = ()=>{
        dispatch(editUserHandler(editUser))
        dispatch(setStatus(false));
    }
  return (
    <div>
      <Modal
        open={openEdit}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Edit User
          </Typography>
          <Typography id="modal-modal-description" sx={{ mt: 2 }}>
            <Grid container spacing={2}>
                <Grid item xs={3}>
                    <p>First Name</p>
                </Grid>
                <Grid item xs={9}>
                    <TextField onChange={handleChange} name="firstname" label={`${user.firstname}`} variant="outlined"fullWidth />
                </Grid>
                <Grid item xs={3}>
                    <p>Last Name</p>
                </Grid>
                <Grid item xs={9}>
                    <TextField name="lastname" onChange={handleChange} label={`${user.lastname}`} variant="outlined" fullWidth/>
                </Grid>
                <Grid item xs={3}>
                    <p>Subject</p>
                </Grid>
                <Grid item xs={9}> 
                    <TextField name="subject" onChange={handleChange} label={`${user.subject}`} variant="outlined" fullWidth/>
                </Grid>
                <Grid item xs={3}>
                    <p>Country</p>
                </Grid>
                <Grid item xs={9}>
                    <InputLabel>Edit Country</InputLabel>
                        <Select
                        onChange={handleChange}
                        fullWidth
                        name="country"
                        value={editUser.country}
                        >
                            <MenuItem value="VN">Việt Nam</MenuItem>
                            <MenuItem value="USA">USA</MenuItem>
                            <MenuItem value="AUS">Australia</MenuItem>
                            <MenuItem value="CAN">Canada</MenuItem>
                    </Select>
                </Grid>
                <Grid item xs={3}>
                    <p>Register Status</p>
                </Grid>
                <Grid item xs={9}>
                    <InputLabel>Edit Register Status</InputLabel>
                        <Select
                        onChange={handleChange}
                        fullWidth
                        name="registerStatus"
                        value={editUser.registerStatus}
                        >
                            <MenuItem value="New">New</MenuItem>
                            <MenuItem value="Accepted">Accepted</MenuItem>
                            <MenuItem value="Denied">Denied</MenuItem>
                            <MenuItem value="Standard">Standard</MenuItem>
                    </Select>
                </Grid>
                <Grid item xs={3}>
                    <p>Country</p>
                </Grid>
                <Grid item xs={9}>
                    <InputLabel>Edit Customer Type</InputLabel>
                        <Select
                        onChange={handleChange}
                        fullWidth
                        name="customerType"
                        value={editUser.customerType}
                        >
                            <MenuItem value="Standard">Standard</MenuItem>
                            <MenuItem value="Gold">Gold</MenuItem>
                            <MenuItem value="Premium">Premium</MenuItem>
                    </Select>
                </Grid>
            </Grid>
          </Typography>
          <Grid container mt={2} spacing={2}
                direction="row"
                justifyContent="flex-end"
                alignItems="center"> 
          <Grid item>
            <Button  onClick={handleClose} variant="outlined">Close</Button>
          </Grid>
          <Grid item>
            <Button onClick={onBtnEditUserHandler} variant="contained"> Edit User</Button>
          </Grid>
            
          </Grid>
        </Box>
      </Modal>
    </div>
  );
}