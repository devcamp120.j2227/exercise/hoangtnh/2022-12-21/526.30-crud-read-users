import { FETCH_PENDING_API, FETCH_SUCCESS_API, FETCH_ERROR_API, GET_USER, ADD_USER_MODAL, SET_STATUS,
    EDIT_USER, CREATE_NEW_USER, EDIT_USER_STATUS, DELETE_USER, DELETE_USER_STATUS
     } from "../constants/users.constants";

const initialState = {
    users:[],
    pending: false,
    error: null, 
    user: null,
    status: false, //**********
    totalUser: 0,
    newUser: {}, //tạo biến newUser để chứa user mới được tạo
    openEdit: false,
    statusCreate: false,
    openDelete: false,
}

export default function userReducer(state = initialState, action){
    switch (action.type) {

        case FETCH_PENDING_API:
            state.pending = true
            break;
            
        case FETCH_SUCCESS_API:
            state.users = action.data;
            state.totalUser = state.users.length;//lấy số lượng users
            state.pending = false
            break;

        case GET_USER:
            state.user = action.data;
            break;
        case ADD_USER_MODAL:
            state.newUser = action.data;
            state.openEdit = false;
            break;
        case CREATE_NEW_USER:
            state.statusCreate = true;
            state.status = action.data;
            break;
        case SET_STATUS:
            state.status = action.data;
            break;

        case EDIT_USER:
            state.openEdit = false;
            break;
        case EDIT_USER_STATUS:
            state.openEdit = action.data;
            break;

        case DELETE_USER_STATUS:
            state.openDelete = action.data;
            break;
        case DELETE_USER:
            state.openDelete = false;
            alert(`Delete user with Id ${state.user.id} successfull`);
            window.location.reload();
        default:
            break;
    }

    return {...state}
}